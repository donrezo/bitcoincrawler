# Quickstart guide

According to details of implementation project contains only 
rest endpoint and command. 

To make it fully functional project should be dockerized and command attach to cron. (to make it executed every 5 min which will result with updating data and saving it in db).
You also need to connect database of choice (Project contains default docker postgresql mock)
Project using external api for btc fetching.

Run project: 

``` bin/console symfony serve ```

Tests:

``` php bin/phpunit ```

Command: 

``` bin/console app:save-btc-rate ```

Endpoint: 

``` http://127.0.0.1:8000/v1/btc/rate ```