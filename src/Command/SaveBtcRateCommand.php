<?php

namespace App\Command;

use App\Repository\BTCExchangeCourseRepository;
use App\Service\BitCoinCrawler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SaveBtcRateCommand extends Command
{
    protected static $defaultName = 'app:save-btc-rate';
    protected static $defaultDescription = 'save json result in db';
    private BitCoinCrawler $bitCoinCrawler;
    private BTCExchangeCourseRepository $BTCExchangeCourseRepository;

    public function __construct(string $name = null,
                                BitCoinCrawler $bitCoinCrawler,
                                BTCExchangeCourseRepository $BTCExchangeCourseRepository
    )
    {
        parent::__construct($name);
        $this->bitCoinCrawler = $bitCoinCrawler;
        $this->BTCExchangeCourseRepository = $BTCExchangeCourseRepository;
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $json = $this->bitCoinCrawler->getCurrentBtcRate(null)->getContent();
        $this->BTCExchangeCourseRepository->saveJson($json);

        $io->success('You have saved BTC rate in database');

        return Command::SUCCESS;
    }
}
