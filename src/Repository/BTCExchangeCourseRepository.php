<?php

namespace App\Repository;

use App\Entity\BTCExchangeCourse;
use App\DataTransformer\BtcJsonToEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<BTCExchangeCourse>
 *
 * @method BTCExchangeCourse|null find($id, $lockMode = null, $lockVersion = null)
 * @method BTCExchangeCourse|null findOneBy(array $criteria, array $orderBy = null)
 * @method BTCExchangeCourse[]    findAll()
 * @method BTCExchangeCourse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BTCExchangeCourseRepository extends ServiceEntityRepository
{
    private BtcJsonToEntity $btcJsonToEntity;

    public function __construct(ManagerRegistry $registry, BtcJsonToEntity $btcJsonToEntity)
    {
        parent::__construct($registry, BTCExchangeCourse::class);
        $this->btcJsonToEntity = $btcJsonToEntity;
    }

    public function add(BTCExchangeCourse $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(BTCExchangeCourse $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }


    public function saveJson($data, bool $flush = true)
    {
        $entity = $this->btcJsonToEntity->transform($data);
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
