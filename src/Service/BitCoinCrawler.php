<?php

namespace App\Service;

use Exception;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;


class BitCoinCrawler
{
    private CurlHttpClient $httpClient;
    private LoggerInterface $logger;

    public function __construct(HttpClientInterface $httpClient, LoggerInterface $logger)
    {
        $this->httpClient = $httpClient;
        $this->logger = $logger;
    }

    /**
     * @param array|null $filter
     * @return ResponseInterface|null
     * @throws TransportExceptionInterface
     */
    public function getCurrentBtcRate(?array $filter): ?ResponseInterface
    {
        try {
            $response = $this->httpClient->request("GET", "https://api.coincap.io/v2/assets");
        } catch (Exception $e) {
            $this->logger->log(LOG_CRIT, $e->getMessage());
            return null;
        }

        return $response;
    }
}