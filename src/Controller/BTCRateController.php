<?php

namespace App\Controller;

use App\DataTransformer\BtcJsonToEntity;
use App\Service\BitCoinCrawler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/v1")
 */
class BTCRateController extends AbstractController
{
    private BitCoinCrawler $bitCoinCrawler;
    private BtcJsonToEntity $btcJsonToEntity;

    public function __construct(BitCoinCrawler $bitCoinCrawler, BtcJsonToEntity $btcJsonToEntity)
    {
        $this->bitCoinCrawler = $bitCoinCrawler;
        $this->btcJsonToEntity = $btcJsonToEntity;
    }

    /**
     * @Route("/btc/rate/{filter}", name="app_btc_rate")
     */
    public function index($filter = null): JsonResponse
    {
        $response = $this->bitCoinCrawler->getCurrentBtcRate($filter);

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $response ? $response = $serializer->serialize($this->btcJsonToEntity->transform($response->getContent()),'json') : "No data";

        return new JsonResponse($response,200,[], true);
    }
}
