<?php

namespace App\DataTransformer;

use App\Entity\BTCExchangeCourse;

class BtcJsonToEntity
{
    public function transform($data): ?BTCExchangeCourse
    {
        $rates = json_decode($data,true);
        foreach ($rates["data"] as $rate)
        {
            if($rate["id"] == "bitcoin")
            {
                $entity = new BTCExchangeCourse();
                $entity->setCurrency("USD");
                $entity->setDate(new \DateTime());
                $entity->setRate($rate["priceUsd"]);
                return $entity;
            }
        }
        return null;
    }
}