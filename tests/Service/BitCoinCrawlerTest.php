<?php declare(strict_types=1);

namespace Service;

use App\Service\BitCoinCrawler;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\CurlHttpClient;

final class BitCoinCrawlerTest extends TestCase
{
    /**
     * @var BitCoinCrawler
     */
    private $bitCoinCrawler;
    /**
     * @var CurlHttpClient
     */
    private $httpClient;
    /**
     * @var Logger
     */
    private $logger;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->httpClient = new CurlHttpClient();
        $this->logger = new Logger("test");
        $this->bitCoinCrawler = new BitCoinCrawler($this->httpClient, $this->logger);
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        $this->httpClient = null;
        $this->bitCoinCrawler = null;
    }

    /**
     * @return void
     */
    public function testGetCurrentBtcRate(): void
    {
        $expected = 200;
        $result = $this->bitCoinCrawler->getCurrentBtcRate(null);

        $this->assertEquals($result->getStatusCode(), $expected);
    }
}